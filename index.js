/**
 * Small node.js / express / mongodb app to serve a file written in markdown and allow the addition of
 * notes and highlights. This is the point of entry.
 *
 * @module index
 * @exports the express app instance
 */

var config = require('./app/config');
var logger = require('./app/logger');

var express = require('express');
var session = require('express-session');
var mongoose = require('mongoose');
var body_parser = require('body-parser');

var app = express();



app.use(body_parser.json());

mongoose.connect(config.db_uri[app.settings.env]);
require('./app/mongoose')(mongoose);

app.use(session({secret: config.cookie_secret,
                        resave: false,
                        saveUninitialized: false,
                        cookie: {
                            httpOnly: false,
                        }
                        }));
                        


var rest_api = require('./app/rest_api')(app, mongoose);
var app = require('./app/routes')(express, app, rest_api);


var server = app.listen(config.server_port[app.settings.env], function () {
    console.log('App listening at http://%s:%s in %s mode.',
    server.address().address, server.address().port, app.settings.env);
});

server.app = app;
server.mongoose = mongoose;

module.exports = server;