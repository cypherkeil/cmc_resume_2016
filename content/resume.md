Christine Chun
--------------
Los Angeles, CA  
(310) 498-7077  
cmchun7@gmail.com


I'm a full-stack Node.js and Python web developer with over 10 years of software development experience. I am looking for a dynamic, creative, and cooperative role developing excellent and elegant web-based user experiences.
{: .emphasis}

### Technical experience
- Node.js
- jQuery
- Python
- Polymer
- MongoDB
- PostgreSQL
- MySQL
- HTML, CSS, and Javascript

### Design
- Graphic design in both print and web media
- Information architecture
- User-centered design
- Human-computer interaction evaluation techniques
- Adobe Photoshop, Lightroom, Illustrator, and InDesign

### Client Interaction
 I have experience working directly with clients to understand and accomplish their business needs in a consulting relationship. 

- Requirements gathering
- Communication and management
- Solutions design
- Design iteration
- Documentation
- Delivery
- Support

Education
----------------

Carnegie Mellon University, Pittsburgh, Pennsylvania  
BS in Computer Science  
BS in Civil and Environmental Engineering with a Minor in Communications Design  
Graduated in May 2001

Work Experience
----------------

Journyx, Inc. Austin, Texas  
Senior Software Engineer  
April 2004 - January 2015  

Worked on all aspects of design, development, testing, debugging, and supporting the company's major web application product in Python with support for multiple database platforms. Implemented a major new product feature integrating resource management features into the core product. Regularly interacted with the development, sales, and support teams as well as directly with clients to address their development and support needs. Initiated a cultural change in the development department to support collaborative design and development, and to integrate a testing workflow into the development process.


Maya Design, Inc., Pittsburgh, Pennsylvania  
Software Engineer  
August 2001 - March 2002; interned from June 2000 - January 2001  

Designed, developed, and implemented a wide-ranging array of interactive media solutions to deliver for a variety of client requirements in a consulting environment. Technologies included Python, Microsoft ASP, Java, Java servlets and JSP, XML, Javascript, HTML, and database-driven applications.








