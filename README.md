## Overview

Quick little Node.js app using Express, Mongoose (MongoDB), and Polymer which displays markdown content and allows the addition of highlights and notes. The page is implemented using a Polymer interface, connected to a RESTful API on the server.

## Tests

Run `mocha` for a series of tests.

## API

### URL

`/:user_email/notes`

### Method

`POST`

### Body Params

Expects an array of objects describing the notes to create.

    [
        {
            serialized: <String representation of the highlight serialization>
        }
    ]

### Success Response

Code: 200  
Content: An array of the created note objects.  

    [
        {
            _id: <String note ID>,
            creator: <String email address of the user>,
            serialized: <String representation of the highlight serialization>
        }
    ]

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

### URL

`/:user_email/notes`

### Method

`GET`

### Body Params

None.

### Success Response

Code: 200  
Content: An array of note objects for the user.  

    [
        {
            _id: <String note ID>,
            creator: <String email address of the user>,
            serialized: <String representation of the highlight serialization>
        }
    ]

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

### URL

`/:user_email/notes`

### Method

`DELETE`

### Body Params

None.

### Success Response

Code: 200  
Content: An empty array.

    []

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

### URL

`/:user_email/notes/:note_id`

### Method

`PUT`

### Body Params

Expects an array of objects describing the notes to create.

    [
        {
            serialized: <String representation of the highlight serialization>
        }
    ]

### Success Response

Code: 200  
Content: An array of the modified note object.  

    [
        {
            _id: <String note ID>,
            creator: <String email address of the user>,
            serialized: <String representation of the highlight serialization>
        }
    ]

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

### URL

`/:user_email/notes/:note_id`

### Method

`GET`

### Body Params

None.

### Success Response

Code: 200  
Content: An array containing the single requested note object.  

    [
        {
            _id: <String note ID>,
            creator: <String email address of the user>,
            serialized: <String representation of the highlight serialization>
        }
    ]

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

### URL

`/:user_email/notes/:note_id`

### Method

`DELETE`

### Body Params

None.

### Success Response

Code: 200  
Content: An empty array. 

    []

### Error Response

Code: 400  
Reason: Invalid email in request URL, or invalid data in request body JSON.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

Code: 500  
Reason: Lower-level server error while performing request.  
Content:

    {
        name: <String error name>,
        message: <String error message>,
        req_method: <String request method>,
        req_url: <String request URL>,
        req_body: <JSON request body data>
    }

