/**
 * Configures the winston logger.
 *
 * @module app/logger
 */
 
module.exports = function() {

    var winston = require('winston');
    
    winston.emitErrs = true;
    winston.remove(winston.transports.Console);
    
    winston.loggers.add("dev", {
        
        console: {
            silent: true,
            level: "error",
            handleExceptions: false,
            json: false,
            colorize: true,  
            exitOnError: false,
        }, 
        
        file: {
            level: "silly",
            filename: "./logs/debug.txt",
            handleExceptions: true,
            json: false,
            maxFiles: 1,
            colorize: true,
            timestamp: function() { return Date().toString().slice(4, 24); },
            prettyPrint: true,
            humanReadableUnhandledException: false,
            exitOnError: false,
        }
    });
   
    // for morgan
    var logger = winston.loggers.get('dev');
    logger.stream = {
        write: function(msg, encoding) {
            logger.info(msg);
        }
    };
    
    
    /*global Writable
    https://github.com/FutureAdLabs/winston-stream/
    */

    var stream = require("stream");
    var util = require("util");

    util.inherits(WinstonStream, stream.Writable);

    function WinstonStream(logger, level) {
        stream.Writable.call(this, {decodeStrings: false});
        this._logger = logger;
        this._level = level;
    }

    WinstonStream.prototype._write = function _write(chunk, encoding, callback) {
        var s = (typeof chunk === "string") ? chunk : chunk.toString("utf-8");
        if (s.slice(-1) === "\n") {
            s = s.slice(0, -1);
        }
        this._logger.log(this._level, s);
        callback();
    }

    var winston_stream = new WinstonStream(logger, "error");
    
    //process.stderr.pipe(winston_stream);
    process.__defineGetter__('stderr', function() { return winston_stream; });
    
    
    return logger;
    
}();