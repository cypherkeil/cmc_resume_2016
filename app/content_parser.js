/**
 * Parses a text file from markdown to HTML
 * 
 * @module app/content_parser
 */
 
module.exports = {
    
    /**
     * Callback to accept the HTML output of markdownToHTML
     *
     * @callback markdownToHTMLCallback
     * @param {Error} err - Error, if any. Otherwise, null.
     * @param {string} html_output - The HTML output of the markdown file. Omitted if there is an error.
     */
     
    /**
     * Reads a file asynchronously in markdown, translates it to an HTML string, and passes the output to a callback.
     * @function
     *
     * @param {string} filename - A string containing the location of the file, relative to this module, containing markdown-formatted text to translate to HTML.
     * @param {markdownToHTMLCallback} callback - The callback used to handle the HTML output.
     */
    markdownToHTML(filename, callback) {
        
        var logger = require('winston').loggers.get('dev');
        var fs = require('fs');
        var path = require('path');
        var markdown = require('markdown').markdown;
        
        fs.readFile(path.join(__dirname, filename), 'utf8', function(err, data) {
            if (err) {
                logger.error(err);
                return callback(err);
            }
            
            var tree = markdown.parse(data, 'Maruku');
            
            var html_tree = markdown.toHTMLTree(tree);
            var html_output = markdown.renderJsonML(html_tree);
            //var html_output = __dirname + markdown.toHTML(data, 'Maruku');
            callback(null, html_output);
        });
        
    },
    
    /**
     * Callback to accept the text output of htmlToText
     *
     * @callback htmlToTextCallback
     * @param {Error} err - Error, if any. Otherwise, null.
     * @param {string} text_output - The text output of the html input. Omitted if there is an error.
     */
     
    /**
     * Reads a file asynchronously in markdown, translates it to an HTML string, and passes the output to a callback.
     * @function
     *
     * @param {string} html_input - A string containing the html to translate.
     * @param {htmlToTextCallback} callback - The callback used to handle the textL output.
     */
    htmlToText(html_input, note_index, callback) {
        var logger = require('winston').loggers.get('dev');
        var htmlparser = require('htmlparser2');
        var text = "";
        
        var ignore_names = ["script", "style"];
        var return_names = ["br", "li"];
        
        var note_i = 0;
        var seen_note_index = {};
        var new_note_index = {};
        
        var tag_name, tag_attribs;
        var ol_count = -1;
        var ul_bullet = "";
        
        var parser = new htmlparser.Parser({
            onopentag: function(name, attribs) {
                tag_name = name;
                tag_attribs = attribs;
                
                if (name == "ul") {
                    ul_bullet = "- ";
                } else if (name == "ol") {
                    ol_count = 1;
                }
                
                if (tag_name == "li") {
                    if (ol_count > -1) {
                        text += ol_count + ". ";
                        ol_count++;
                    } else if (ul_bullet) {
                        text += ul_bullet;
                    }
                }
                
                if (attribs.hasOwnProperty("highlight_id")) {
                    var hl_id = attribs.highlight_id;
                    
                    if (!seen_note_index.hasOwnProperty(hl_id)) {
                        seen_note_index[hl_id] = ++note_i;
                        
                        var note = "";
                        if (note_index.hasOwnProperty(hl_id)) {
                            note = note_index[hl_id];
                        }
                        new_note_index[note_i] = note;
                    }
                    text += "[";
                }
            },
            
            ontext: function(text_content) {
                if (ignore_names.indexOf(tag_name) != -1) {
                    return;
                }
                
                
                text += text_content;
            },
            
            onclosetag: function(name) {
                if (tag_attribs.hasOwnProperty("highlight_id")) {
                    var hl_id = tag_attribs.highlight_id;
                    
                    text += "](" + note_i + ")";
                }
                
                if (return_names.indexOf(name) != -1) {
                        text += "\n";
                }
                
                if (name == "ul") {
                    ul_bullet = "";
                } else if (name == "ol") {
                    ol_count = -1;
                }
                
                tag_name = "";
                tag_attribs = {};
            },
        }, 
        
        {decodeEntities: true,
          lowerCaseTags: true});
        
        parser.parseComplete(html_input);
        
        var note_text = "";
        
        for (var i=1; i<=note_i; i++) {
            note_text += "(" + i + ") " + new_note_index[i] + "\n";
        }
        
        if (note_text) {
            text = note_text + "\n-------\n" + text;
        }
        
        callback(null, text);
    },

};