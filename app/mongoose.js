/**
 * Configures mongoose as an ORM to setup the data objects needed.
 *
 * @module app/mongoose
 *
 * @param {module} mongoose - A reference to the mongoose module
 */
 
module.exports = function(mongoose) {
    
    mongoose.Promise = Promise;
    
    var logger = require('winston').loggers.get('dev');

    // begin mongoose setup
    
    var utils = require('util');
    var db = mongoose.connection;

    db.on('error', logger.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log('mongodb connected');
    });


    // data model

    /* Two types of "details" setup as discriminators and the base Detail object:
     * Highlights, which simply select a portion of the document
     * Notes, which select a portion of the document and attach a text note.
     */
     
    var schema_options = {
        discriminatorKey: 'type'
    }

    var creator_fields = {
        email: String,
        create_date: { type: Date, default: Date.now },
    }
    
    var CreatorSchema = new mongoose.Schema(creator_fields);
    
    var highlight_fields = {
        creator: {type: String, required: [true, "No creator set."]},
        serialized: {type: String, required: [true, "No serialization specified."]},
        create_date: { type: Date, default: Date.now },
        modify_date: { type: Date, default: Date.now }
        
    }
    
    var note_fields = utils._extend({note: String},
                                               highlight_fields);
                                               
    
    // details are the base model for notes and highlights
    var DetailSchema = new mongoose.Schema(highlight_fields, schema_options);
    var NoteSchema = new mongoose.Schema(note_fields, schema_options);
    var HighlightSchema = new mongoose.Schema(highlight_fields, schema_options);
    

    // add schema methods
 
    // populate "creator" in find queries
    /*
    var populateQueryCreator = function(next) {
        
        this.populate("creator");
        
        next();
    }
    
    DetailSchema.pre('find', populateQueryCreator);
    DetailSchema.pre('findOne', populateQueryCreator);
    */
    
    // update modify_date whenever a detail is modified
    var updateQueryModifyDate = function(next) {
        this.update({$set: {modify_date: new Date() }});
        
        next();
    };
    
    var updateDocModifyDate = function(next) {
        var obj = this;
        
        obj.modify_date = new Date();
        
        next();
    };
    
    DetailSchema.pre('save', updateDocModifyDate);
    DetailSchema.pre('update', updateQueryModifyDate);
    DetailSchema.pre('findOneAndUpdate', updateQueryModifyDate);
    
    
    // if we delete a creator, delete all their notes, too
    CreatorSchema.post('remove', function(doc) {
        
        var detail_model = mongoose.model("Detail");
        
        detail.mode.remove({creator: doc._id}, function(err, count) {
            if (err) return logger.error(err);
            
            logger.info("All " + JSON.stringify(count) + " details for creator " + doc._id + " have been removed.");
        });
    });
    
    
    var Creator = mongoose.model("Creator", CreatorSchema);
    
    var Detail = mongoose.model("Detail", DetailSchema);
    var Note = Detail.discriminator("Note", NoteSchema);
    var Highlight = Detail.discriminator("Highlight", HighlightSchema);

};