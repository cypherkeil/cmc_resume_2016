/**
 * Defines the express routes that establish the REST api.
 *
 * @module app/rest_api
 *
 * @param {instance} app - The express instance representing the application to which routes should be added.
 * @param {module} mongoose - A reference to the mongoose module.
 */
 
module.exports = function(app, mongoose) {
    
    // defines the rest api ...
    
    var logger = require('winston').loggers.get('dev');
    var sanitize_html = require('sanitize-html');
    var validator = require('is-my-json-valid');
    var validation_schema = require('./validation_schema');
    
    var db = mongoose.connection;
    
    /**
     * Function for handling invalid data when checked by the validator.
     *
     * @param {Array} validator_errors - A reference to an is-my-json-valid validator errors attribute. Assumes there are errors to be handled.
     * @param {Object} req - The express request object.
     * @param {Object} res - The express response object.
     * 
     * @return {Object} error_object - Returns a javascript object defined by errorHandlerJSON encapsulating the validation errors encountered.
     */
    function errorHandlerValidator(validator_errors, req, res) {
        
        var msg = validator_errors.map(function(obj, i) {
            return obj.field + " " + obj.message;
        }).join(", ");

        return errorHandlerJSON({
            name: "Invalid request data",
            message: msg
        }, req, res);
        
    }
    
    /**
     * Function for handling errors and creating a standardized object containing error information.
     *
     * @param {Error} err - A javascript Error.
     * @param {Object} req - The express request object.
     * @param {Object} res - The express response object.
     *
     * @return {Object} error_object - An object with attributes containing information relevant to the error.
     */
    function errorHandlerJSON(err, req, res) {
        var err_name = err.name;
        var err_msg = err.message;
        var err_stack = err.stack;
        
        var req_method = req.method;
        var req_url = req.originalUrl;
        var req_route = req.route;
        var req_body = req.body;
        
        var err_obj = {
            name: err_name,
            message: err_msg,
            stacktrace: err_stack,
            req_method: req_method,
            req_url: req_url,
            req_body: req_body
        }
        logger.error("Error: " + err_name + " : " + err_msg, err_obj);
        
        return err_obj;
    }
    
    /**
     * Express middleware for ensuring there is a valid email address is the URI
     * The email address indicates the "user" resource.
     * There are NO data (details, highlights, notes) saved without an associated user.
     *
     * @param {Object} req - The express request object.
     * @param {Object} res - The express response object.
     * @param {function} next - Function that passes control to the next middleware.
     */
    var validUserURL_MW = function(req, res, next) {
        var user_id = req.params.user_id;
        
        user_id = sanitize_html(user_id);
        
        var validate = validator(validation_schema.url_email);
            
        if (!validate({url_user: user_id})) {
            return res.status(400).json(errorHandlerValidator(validate.errors, req, res));
        }
        
        if (req.body.constructor === Array) {
            req.body.forEach(function(data_obj, i) {
                data_obj["creator"] = user_id;
                req.body[i] = data_obj;
            });
        }
        
        next();
    };
    
    
    
    // media objects
    /*
    /details POST
    /details GET
    /details DELETE
    
    /details/:id PUT update
    /details/:id GET
    /details/:id DELETE
    
    /highlight POST create
    /highlight GET get all
    /highlight DELETE delete all
    
    /highlight/:id PUT update
    /highlight/:id GET
    /highlight/:id DELETE
    */
    
    [{name: "Detail", route: "/details", singular: "detail", plural: "details"},
        {name: "Highlight", route: "/highlights", singular: "highlight", plural: "highlights"},
            {name: "Note", route: "/notes", singular: "note", plural: "notes"}].map(function(model_dict) {
        
        var model_obj = mongoose.model(model_dict.name);
        var route = model_dict.route;
        var route_with_user = "/:user_id" + route;
        
        // establish user resource in the URI for these routes
        app.use(route_with_user, validUserURL_MW);
        app.use(route_with_user + ":obj_id", validUserURL_MW);
        
        /**
         * Creates new details for this user.
         *
         * @function POST /:user_email/details
         * 
         * @param {Array} detail_objects - Accepts an JSON array of objects describing the detail objects to be created.
         * @param {string} detail_objects.creator - Email address of the user that owns this detail. This attribute is automatically created by the validUserURL_MW middleware.
         * @param {string} detail_objects.serialized - Serialization of the DOM highlight data.
         *
         * @return {Array} detail_objects - Emits a JSON response of an array of the created detail objects, including unique _id attributes.
         */
        app.post(route_with_user, function(req, res) {
            var validate = validator(validation_schema.detail);
            
            if (!validate(req.body)) {
                return res.status(400).json(errorHandlerValidator(validate.errors, req, res));
            }
            
            var user_id = req.params.user_id;
            var detail_docs = [];
            
            req.body.forEach(function(detail_obj, i) {
                
                detail_obj.note = sanitize_html(detail_obj.note);
                var new_obj = new model_obj(detail_obj);
                //new_obj.creator = req.user._id;
                
                detail_docs.push(new_obj.save());
            });
            
            Promise.all(detail_docs)
                .then(function(saved_docs) {
                    res.json(saved_docs);
                }, function(err) {
                    return res.status(500).json(errorHandlerJSON(err, req, res));
                });
        });
        
        /**
         * Gets all details for this user.
         *
         * @function GET /:user_email/details
         *
         * @return {Array} detail_objects - Emits a JSON response of an array of the detail objects for this user.
         */
        app.get(route_with_user, function (req, res) {
            
            var user_id = req.params.user_id;
            
            model_obj.find({creator: user_id}).lean().exec(function(err, objs) {
                if (err) return res.status(500).json(errorHandlerJSON(err, req, res));
                
                return res.json(objs);
            });
        });
        
        /**
         * Deletes all details for this user.
         *
         * @function DELETE  /:user_email/details
         *
         * @return {Array} detail_objects - Emits a JSON response of an empty array.
         */
        app.delete(route_with_user, function(req, res) {
            
            var user_id = req.params.user_id;
            
            model_obj.find({creator: user_id}, function(err, objs) {
                if (err) return res.status(500).json(errorHandlerJSON(err, req, res));
                
                var rm_arr = [];
                
                objs.forEach(function(obj) {
                    rm_arr.push(obj.remove());
                })
                
                Promise.all(rm_arr)
                    .then(function(rm_responses) {
                        return res.json([]);
                    }, function(err) {
                        return res.status(500).json(errorHandlerJSON(err, req, res));
                    });
            });
            
        });
        
        /**
         * Modifies the single specified detail object.
         *
         * @function PUT /:user_email/details/:obj_id
         * 
         * @param {Array} detail_object - Accepts an JSON array containing the single detail object describing the attributes to the modified.
         * @param {string} detail_object.creator - Email address of the user that owns this detail. This attribute is automatically created by the validUserURL_MW middleware.
         * @param {string} detail_object.serialized - Serialization of the DOM highlight data.
         *
         * @return {Array} detail_object - Emits a JSON response of an array with a single member containing modified detail object.
         */
        app.put(route_with_user + '/:obj_id', function(req, res) {
            var user_id = req.params.user_id;
            var obj_id = req.params.obj_id;
            var validate = validator(validation_schema.detail);
            
            if (!validate(req.body)) {
                return res.status(400).json(errorHandlerValidator(validate.errors, req, res));
            }
            
            var docs = [];
            
            req.body.forEach(function(note_obj, i) {
                note_obj.note = sanitize_html(note_obj.note);
                var query = {_id: obj_id};
                
                // make sure modify_date is not set or it won't update
                delete(note_obj["modify_date"]);
                
                docs.push(model_obj.findOneAndUpdate(query, note_obj, {new: true}).exec());
            });
            
            Promise.all(docs)
                .then(function(saved_docs) {
                    res.json(saved_docs);
                }, function(err) {
                    return res.status(500).json(errorHandlerJSON(err, req, res));
                });
        });
        
        /**
         * Returns the single requested detail object.
         *
         * @function GET /:user_email/details/:obj_id
         *
         * @return {Array} detail_object - Emits a JSON response of an array with a single member containing the requested detail object.
         */
        app.get(route_with_user + '/:obj_id', function(req, res) {
            
            var user_id = req.params.user_id;
            var obj_id = req.params.obj_id;
            
            model_obj.find({_id: obj_id}).lean().exec(function(err, objs) {
                if (err) return res.status(500).json(errorHandlerJSON(err, req, res));
                
                return res.json(objs);
            });
        });
        
        /**
         * Deletes the specified detail.
         *
         * @function DELETE /:user_email/details/:obj_id
         *
         * @return {Array} detail_object - Emits a JSON response of an empty array.
         */
        app.delete(route_with_user + '/:obj_id', function(req, res) {
            
            var user_id = req.params.user_id;
            var obj_id = req.params.obj_id;
            
            model_obj.findOne({_id: obj_id}, function(err, obj) {
                if (err) return res.status(500).json(errorHandlerJSON(err, req, res));
                
                if (obj) obj.remove();
                
                return res.json([]);
            });
        });
        
    });        
        
   var exports = {
       
       errorHandlerValidator: errorHandlerValidator,
       errorHandlerJSON: errorHandlerJSON,
   };
   
   return exports;
};
