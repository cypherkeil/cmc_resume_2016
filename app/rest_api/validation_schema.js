/**
 * JSON schema for data validation
 *
 * The "url_email" schema describes an object with an "url_user" property which is in email format.
 * "detail" and "note" describe detail/highlight fields, and note fields expected in the JSON body, respectively.
 *
 * @module app/rest_api/validation_schema
 */
module.exports = {
    url_email: {
                type: "object",
                properties: {
                    url_user: {
                        type: "string",
                        required: true,
                        format: "email"
                    },
                }
             },
                
    detail: {
                type: "array",
                required: true,
                items: {
                    type: "object",
                    properties: {
                        
                        creator: {
                            type: "string",
                            required: true,
                            format: "email"
                        },
                        
                        serialized: {
                            type: "string",
                            required: true,
                        },
                    } // array object properties
                } // items
            }, // detail
            
    note: {
                type: "array",
                required: true,
                items: {
                    type: "object",
                    properties: {
                        
                        creator: {
                            type: "string",
                            required: true,
                            format: "email"
                        },
                        
                        serialized: {
                            type: "string",
                            required: true,
                        },
                        note: {
                            type: "string",
                        },
                    } // array object properties
                } // items
            }, // note
            
    html_input: {
                type: "object",
                required: true,
                properties: {
                    html_input: {
                        type: "string",
                        required: true,
                    },
                    note_index: {
                        type: "object",
                    },
                }, // properties
    }, // html_input
};