/**
 * Adds routes unrelated to the REST api to the express app. Further routes are added in polymer/index.js.
 *
 * @module app/routes
 *
 * @param {module} express - A reference to the express module
 * @param {instance} app - The express app instance to which the routes should be added
 *
 * @returns {instance} app - Returns the same express app instance which was passed in.
 */
 
module.exports = function(express, app, rest_api) {
    
    var content_parser = require('./content_parser');
    
    // Setup polymer for the client-facing javascript
    require('../polymer')(express, app);
    
    // Serve the content directory as static content
    app.use('/content', express.static("content"));
    
    // Serve the HTML conversion of the markdown-formatted file to be used as content for the app
    app.get("/content/html", function(req, res) {
        
        content_parser.markdownToHTML('../content/resume.md', function(err, html_output) {
            if (err) {
                logger.error(err);
                res.status(500).send("An error occurred");
            }
            
            res.send(html_output);
        });
        
    });
    
    app.post("/content/text", function(req, res) {
        var html_sanitizer = require('sanitize-html');
        var validator = require('is-my-json-valid');
        var validation_schema = require('./rest_api/validation_schema');
        var validate = validator(validation_schema.html_input);
            
        if (!validate(req.body)) {
            return res.status(400).json(rest_api.errorHandlerValidator(validate.errors, req, res));
        }
        
        var html_input = html_sanitizer(req.body.html_input);
        var note_index = {};
        
        if (req.body.note_index) {
            var note_index = req.body.note_index;
            for (var key in note_index) {
                if (note_index.hasOwnProperty(key)) {
                    note_index[key] = html_sanitizer(note_index[key]);
                }
            }
        }
        
        content_parser.htmlToText(req.body.html_input, note_index, function(err, text_output) {
            if (err) {
                logger.error(err);
                res.status(500).send("An error occurred");
            }
            
            res.send(text_output);
        });
        
    });
    
    
    return app;
    
};
