
module.exports = function(express, app) {

    var path = require('path');
    
    app.use("/bower_components", express.static(path.join(__dirname, "/bower_components")));
    
    var ieResponse = function(req, res, next) {
        var user_agent = req.headers['user-agent'];
        
        if (user_agent.indexOf("MSIE") != -1 || user_agent.indexOf("Trident/") != -1 || user_agent.indexOf("Edge") != -1 || user_agent.indexOf("iPhone") != -1) {
            res.redirect("/html");
        } else {
            next();
        }
    };

    app.get("/", ieResponse, function(req, res) {
        res.sendFile(path.join(__dirname, "/index.html"));
    });
    
    app.get("/html", function(req, res) {
        console.log(__dirname);
        res.sendFile(path.join(__dirname, "../content/resume_2016.html"));
    });
    
    return app;
};