
/**
 * Mocha/chai test for the REST api 
 * @module test/test-rest-api
 */

process.env.NODE_ENV = 'test'

var chai = require('chai');
var server = require('../index.js');
var config = require('../app/config');
var mongoose = server.mongoose;

var logger = require('winston').loggers.get('dev');

var should = chai.should();
var expect = chai.expect();

chai.use(require('chai-http'));

/** 
 * @name testHelper
 * Encapsulates reusable testing functions.
 */
var testHelper = {
    
    agent: chai.request.agent(server),
    test_user_id: config.test_local.user_id,
    
    /**
     * Callback to accept a chai.request.agent instance
     *
     * @callback testHelperLoginCallback
     * @param {chai.request.agent} agent An instance of the chai.request.agent with logged-in test credentials
     */
     
    /**
     * Asynchronously logs the agent attribute of testHelper into the server with the test account
     *
     * @function login
     * @param {testHelperLoginCallback} cb the callback function to use when the login is finished. Accepts the
     * agent object with logged-in credentials as the only argument.
     */  
    
    checkElementList(list1, list2) {
        /* check to make sure two lists of objects are essentially identical
        attr_list = [{name: "name", type: "string"} ...]
        */
     
        list1.length.should.equal(list2.length);
        
        if (list1.length == 0 || list2.length == 0) { return; }
        
        list1.forEach(function (list1_obj, i) {
            
            var list2_obj = list2.filter(function(obj) {
                return (obj._id.toString() == list1_obj._id.toString());
            })[0];

            list2_obj.should.exist;
            
            //list1_obj._id.toString().should.equal(list2_obj._id.toString());
            list1_obj.should.have.property('_id').which.is.not.empty;
            list2_obj.should.have.property('_id').which.is.not.empty;
            
            list1_obj.should.have.property('serialized').that.is.a('string').which.is.not.empty;
            list2_obj.should.have.property('serialized').that.is.a('string').which.is.not.empty;
            
            list1_obj.serialized.should.equal(list2_obj.serialized);
            
        });
    },
    
    checkElementListWithoutIDs(list1, list2) {
        /* check to make sure two lists of objects are essentially identical
        attr_list = [{name: "name", type: "string"} ...]
        */
        
        if (list1.length == 0 || list2.length == 0) { return; }
        
        list1.forEach(function (list1_obj, i) {
            
            var list2_obj = list2.filter(function(obj) {
                return (obj.serialized == list1_obj.serialized);
            })[0];

            list2_obj.should.exist;
            
            list1_obj.should.have.property('serialized').that.is.a('string').which.is.not.empty;
            list2_obj.should.have.property('serialized').that.is.a('string').which.is.not.empty;
            
            list1_obj.serialized.should.equal(list2_obj.serialized);
        });
        
    },
    
};


var testDetails = function () {
    
    [testHelper.test_user_id, encodeURIComponent(testHelper.test_user_id)].forEach(function(user_id, user_i) {
        
        [{name: "Detail", route: "/details", singular: "detail", plural: "details"},
            {name: "Highlight", route: "/highlights", singular: "highlight", plural: "highlights"},
                {name: "Note", route: "/notes", singular: "note", plural: "notes"}].forEach(function(model_dict, i) {
            
            var model_obj = mongoose.model(model_dict.name);
            var route = model_dict.route;
            
            var test_details = [];
            var agent = testHelper.agent;
            var route_with_user = "/" + user_id + route;
            
            describe("testing " + route_with_user, function() {

                before(function(done) {
                    model_obj.collection.drop();
                    done();  
                });


                after(function(done) {
                    model_obj.collection.drop();
                    done();
                });


                it('should list no ' + model_dict.plural + ' on ' + route_with_user + ' GET', function(done) {
                    agent
                    .get(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(0);

                        done();
                    });
                }); // it
                
                it('should fail to add a ' + model_dict.singular + ' without a "serialized" field on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {dummy: "test0"
                                         };
                    
                    var test_arr = [new_obj];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                it('should add a ' + model_dict.singular + ' on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {serialized: "test1"
                                         };
                    
                    var test_arr = [new_obj];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_arr.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementListWithoutIDs(res.body, test_arr);
                        
                        test_details = test_details.concat(res.body);
                        
                        done();
                    });
                }); // it
                
                it('should add two ' + model_dict.plural + ' on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {serialized: "test2",
                                         };
                                         
                    var new_obj2 = {serialized: "test3",
                                        };
                    
                    var test_arr = [new_obj, new_obj2];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj, new_obj2])
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_arr.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementListWithoutIDs(res.body, test_arr);
                        
                        test_details = test_details.concat(res.body);
                        
                        done();
                    });
                }); // it
                
                it('should modify the second ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id PUT', function(done) {
                    
                    var mod_obj = test_details[1];
                    mod_obj.serialized = "modified test 2";
                    
                    var put_route = route_with_user + "/" + mod_obj._id;
                    
                    var test_arr = [mod_obj];
                    
                    agent
                    .put(put_route)
                    .send([mod_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_arr.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementList(res.body, test_arr);
                        
                        test_details[1] = res.body[0];
                        
                        done();
                    });
                }); // it
                
                it('should get all ' + model_dict.plural + ' on ' + route_with_user + ' GET', function(done) {
                    
                    agent
                    .get(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_details.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementList(res.body, test_details);
                        
                        done();
                    });
                }); // it
                
                it('should get the requested ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id GET', function(done) {
                    
                    var get_obj = test_details[2];
                    
                    var get_route = route_with_user + "/" + get_obj._id;
                    
                    var test_arr = [get_obj];
                    
                    agent
                    .get(get_route)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_arr.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementList(res.body, test_arr);
                        
                        done();
                    });
                }); // it
                
                it('should delete the requested ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id DELETE', function(done) {
                    
                    var del_obj = test_details[2];
                    
                    var del_route = route_with_user + "/" + del_obj._id;
                    
                    test_details.splice(2, 1);
                            
                    agent
                    .delete(del_route)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(0);

                        // perform a get and check against modified array
                        
                        agent
                        .get(route_with_user)
                        .end(function(err, res){
                            if (err) console.error(err);
                            
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.an('array');
                            res.body.should.have.length(test_details.length);
                            
                            // check all test objects exist and only test objects exist
                            testHelper.checkElementList(res.body, test_details);
                        
                            done();
                        });
                    });
                }); // it
                
                it('should delete all ' + model_dict.plural + ' on ' + route_with_user + ' DELETE', function(done) {
                    
                    var test_arr = [];
                            
                    agent
                    .delete(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(0);

                        // perform a get and check against modified array
                        
                        agent
                        .get(route_with_user)
                        .end(function(err, res){
                            if (err) console.error(err);
                            
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.an('array');
                            res.body.should.have.length(test_arr.length);
                            
                            // check all test objects exist and only test objects exist
                            testHelper.checkElementList(res.body, test_arr);
                        
                            done();
                        });
                    });
                }); // it
                
                
            }); // describe
        }); // forEach
    }); // user_id forEach
    
    // testing invalid emails
    ["invalid_user", "blah$fwefw!"].forEach(function(user_id, user_i) {
        
        [{name: "Detail", route: "/details", singular: "detail", plural: "details"},
            {name: "Highlight", route: "/highlights", singular: "highlight", plural: "highlights"},
                {name: "Note", route: "/notes", singular: "note", plural: "notes"}].forEach(function(model_dict, i) {
            
            var model_obj = mongoose.model(model_dict.name);
            var route = model_dict.route;
            
            var test_details = [];
            var agent = testHelper.agent;
            var route_with_user = "/" + user_id + route;
            
            describe("testing " + route_with_user, function() {

                before(function(done) {
                    model_obj.collection.drop();
                    done();  
                });


                after(function(done) {
                    model_obj.collection.drop();
                    done();
                });


                it('should fail when listing no ' + model_dict.plural + ' on ' + route_with_user + ' GET', function(done) {
                    agent
                    .get(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');

                        done();
                    });
                }); // it
                
                it('should fail to add a ' + model_dict.singular + ' without a "serialized" field on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {dummy: "test0"
                                         };
                    
                    var test_arr = [new_obj];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                it('should fail to add a ' + model_dict.singular + ' on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {serialized: "test1"
                                         };
                    
                    var test_arr = [new_obj];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);
                        
                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                it('should fail to add two ' + model_dict.plural + ' on ' + route_with_user + ' POST', function(done) {
                    var new_obj = {serialized: "test2",
                                         };
                                         
                    var new_obj2 = {serialized: "test3",
                                        };
                    
                    var test_arr = [new_obj, new_obj2];
                    
                    agent
                    .post(route_with_user)
                    .send([new_obj, new_obj2])
                    .end(function(err, res) {

                        if (err) console.error(err);
                        
                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                /*
                it('should fail to modify the second ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id PUT', function(done) {
                    
                    var mod_obj = test_details[1];
                    mod_obj.serialized = "modified test 2";
                    
                    var put_route = route_with_user + "/" + mod_obj._id;
                    
                    var test_arr = [mod_obj];
                    
                    agent
                    .put(put_route)
                    .send([mod_obj])
                    .end(function(err, res) {

                        if (err) console.error(err);
                        
                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                */
                
                it('should fail to get all ' + model_dict.plural + ' on ' + route_with_user + ' GET', function(done) {
                    
                    agent
                    .get(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);
                        
                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                /*
                it('should fail to get the requested ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id GET', function(done) {
                    
                    var get_obj = test_details[2];
                    
                    var get_route = route_with_user + "/" + get_obj._id;
                    
                    var test_arr = [get_obj];
                    
                    agent
                    .get(get_route)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.an('array');
                        res.body.should.have.length(test_arr.length);

                        // check all test objects exist and only test objects exist
                        testHelper.checkElementList(res.body, test_arr);
                        
                        done();
                    });
                }); // it
                
                it('should fail to delete the requested ' + model_dict.singular + ' on ' + route_with_user + '/:obj_id DELETE', function(done) {
                    
                    var del_obj = test_details[2];
                    
                    var del_route = route_with_user + "/" + del_obj._id;
                    
                    test_details.splice(2, 1);
                            
                    agent
                    .delete(del_route)
                    .end(function(err, res) {

                        if (err) console.error(err);
                        
                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                */
                
                it('should fail to delete all ' + model_dict.plural + ' on ' + route_with_user + ' DELETE', function(done) {
                    
                    var test_arr = [];
                            
                    agent
                    .delete(route_with_user)
                    .end(function(err, res) {

                        if (err) console.error(err);

                        res.should.have.status(400);
                        res.should.be.json;
                        res.body.should.be.an('object');
                        
                        done();
                    });
                }); // it
                
                
            }); // describe
        }); // forEach detail types
    }); // forEach for invalid users
        
}(); // function testDetails